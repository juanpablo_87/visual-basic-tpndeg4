﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.productotxt = New System.Windows.Forms.TextBox()
        Me.codtxt = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.canttxt = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.preciotxt = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.preciotxt2 = New System.Windows.Forms.TextBox()
        Me.canttxt2 = New System.Windows.Forms.TextBox()
        Me.codtxt2 = New System.Windows.Forms.TextBox()
        Me.productotxt2 = New System.Windows.Forms.TextBox()
        Me.preciotxt3 = New System.Windows.Forms.TextBox()
        Me.canttxt3 = New System.Windows.Forms.TextBox()
        Me.codtxt3 = New System.Windows.Forms.TextBox()
        Me.productotxt3 = New System.Windows.Forms.TextBox()
        Me.preciotxt4 = New System.Windows.Forms.TextBox()
        Me.canttxt4 = New System.Windows.Forms.TextBox()
        Me.codtxt4 = New System.Windows.Forms.TextBox()
        Me.productotxt4 = New System.Windows.Forms.TextBox()
        Me.semanatxt = New System.Windows.Forms.ComboBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.totdesctxt = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.prectottxt = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(69, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Producto"
        '
        'productotxt
        '
        Me.productotxt.Location = New System.Drawing.Point(60, 51)
        Me.productotxt.Name = "productotxt"
        Me.productotxt.Size = New System.Drawing.Size(73, 20)
        Me.productotxt.TabIndex = 1
        '
        'codtxt
        '
        Me.codtxt.Location = New System.Drawing.Point(157, 51)
        Me.codtxt.Name = "codtxt"
        Me.codtxt.Size = New System.Drawing.Size(73, 20)
        Me.codtxt.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(169, 21)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Codigo"
        '
        'canttxt
        '
        Me.canttxt.Location = New System.Drawing.Point(250, 51)
        Me.canttxt.Name = "canttxt"
        Me.canttxt.Size = New System.Drawing.Size(73, 20)
        Me.canttxt.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(257, 21)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(49, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Cantidad"
        '
        'preciotxt
        '
        Me.preciotxt.Location = New System.Drawing.Point(345, 51)
        Me.preciotxt.Name = "preciotxt"
        Me.preciotxt.Size = New System.Drawing.Size(73, 20)
        Me.preciotxt.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(353, 21)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Precio U."
        '
        'preciotxt2
        '
        Me.preciotxt2.Location = New System.Drawing.Point(345, 87)
        Me.preciotxt2.Name = "preciotxt2"
        Me.preciotxt2.Size = New System.Drawing.Size(73, 20)
        Me.preciotxt2.TabIndex = 11
        '
        'canttxt2
        '
        Me.canttxt2.Location = New System.Drawing.Point(250, 87)
        Me.canttxt2.Name = "canttxt2"
        Me.canttxt2.Size = New System.Drawing.Size(73, 20)
        Me.canttxt2.TabIndex = 10
        '
        'codtxt2
        '
        Me.codtxt2.Location = New System.Drawing.Point(157, 87)
        Me.codtxt2.Name = "codtxt2"
        Me.codtxt2.Size = New System.Drawing.Size(73, 20)
        Me.codtxt2.TabIndex = 9
        '
        'productotxt2
        '
        Me.productotxt2.Location = New System.Drawing.Point(60, 87)
        Me.productotxt2.Name = "productotxt2"
        Me.productotxt2.Size = New System.Drawing.Size(73, 20)
        Me.productotxt2.TabIndex = 8
        '
        'preciotxt3
        '
        Me.preciotxt3.Location = New System.Drawing.Point(345, 129)
        Me.preciotxt3.Name = "preciotxt3"
        Me.preciotxt3.Size = New System.Drawing.Size(73, 20)
        Me.preciotxt3.TabIndex = 15
        '
        'canttxt3
        '
        Me.canttxt3.Location = New System.Drawing.Point(250, 129)
        Me.canttxt3.Name = "canttxt3"
        Me.canttxt3.Size = New System.Drawing.Size(73, 20)
        Me.canttxt3.TabIndex = 14
        '
        'codtxt3
        '
        Me.codtxt3.Location = New System.Drawing.Point(157, 129)
        Me.codtxt3.Name = "codtxt3"
        Me.codtxt3.Size = New System.Drawing.Size(73, 20)
        Me.codtxt3.TabIndex = 13
        '
        'productotxt3
        '
        Me.productotxt3.Location = New System.Drawing.Point(60, 129)
        Me.productotxt3.Name = "productotxt3"
        Me.productotxt3.Size = New System.Drawing.Size(73, 20)
        Me.productotxt3.TabIndex = 12
        '
        'preciotxt4
        '
        Me.preciotxt4.Location = New System.Drawing.Point(345, 169)
        Me.preciotxt4.Name = "preciotxt4"
        Me.preciotxt4.Size = New System.Drawing.Size(73, 20)
        Me.preciotxt4.TabIndex = 19
        '
        'canttxt4
        '
        Me.canttxt4.Location = New System.Drawing.Point(250, 169)
        Me.canttxt4.Name = "canttxt4"
        Me.canttxt4.Size = New System.Drawing.Size(73, 20)
        Me.canttxt4.TabIndex = 18
        '
        'codtxt4
        '
        Me.codtxt4.Location = New System.Drawing.Point(157, 169)
        Me.codtxt4.Name = "codtxt4"
        Me.codtxt4.Size = New System.Drawing.Size(73, 20)
        Me.codtxt4.TabIndex = 17
        '
        'productotxt4
        '
        Me.productotxt4.Location = New System.Drawing.Point(60, 169)
        Me.productotxt4.Name = "productotxt4"
        Me.productotxt4.Size = New System.Drawing.Size(73, 20)
        Me.productotxt4.TabIndex = 16
        '
        'semanatxt
        '
        Me.semanatxt.FormattingEnabled = True
        Me.semanatxt.Items.AddRange(New Object() {"Lunes", "Martes", "Miercoles", "Jueves", "Sabado", "Domingo"})
        Me.semanatxt.Location = New System.Drawing.Point(12, 236)
        Me.semanatxt.Name = "semanatxt"
        Me.semanatxt.Size = New System.Drawing.Size(121, 21)
        Me.semanatxt.TabIndex = 20
        Me.semanatxt.Text = "Dias"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(27, 301)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 21
        Me.Button1.Text = "Calcular"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'totdesctxt
        '
        Me.totdesctxt.BackColor = System.Drawing.Color.White
        Me.totdesctxt.Location = New System.Drawing.Point(345, 252)
        Me.totdesctxt.Name = "totdesctxt"
        Me.totdesctxt.ReadOnly = True
        Me.totdesctxt.Size = New System.Drawing.Size(113, 20)
        Me.totdesctxt.TabIndex = 22
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(269, 217)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(64, 13)
        Me.Label6.TabIndex = 25
        Me.Label6.Text = "Precio Total"
        '
        'prectottxt
        '
        Me.prectottxt.BackColor = System.Drawing.Color.White
        Me.prectottxt.Location = New System.Drawing.Point(345, 214)
        Me.prectottxt.Name = "prectottxt"
        Me.prectottxt.ReadOnly = True
        Me.prectottxt.Size = New System.Drawing.Size(113, 20)
        Me.prectottxt.TabIndex = 24
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(236, 255)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(97, 13)
        Me.Label7.TabIndex = 27
        Me.Label7.Text = "P. Total Con  Desc"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(123, 301)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 28
        Me.Button2.Text = "Salir"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(9, 214)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(127, 13)
        Me.Label5.TabIndex = 29
        Me.Label5.Text = "Elegir el dia de la semana"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(484, 349)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.prectottxt)
        Me.Controls.Add(Me.totdesctxt)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.semanatxt)
        Me.Controls.Add(Me.preciotxt4)
        Me.Controls.Add(Me.canttxt4)
        Me.Controls.Add(Me.codtxt4)
        Me.Controls.Add(Me.productotxt4)
        Me.Controls.Add(Me.preciotxt3)
        Me.Controls.Add(Me.canttxt3)
        Me.Controls.Add(Me.codtxt3)
        Me.Controls.Add(Me.productotxt3)
        Me.Controls.Add(Me.preciotxt2)
        Me.Controls.Add(Me.canttxt2)
        Me.Controls.Add(Me.codtxt2)
        Me.Controls.Add(Me.productotxt2)
        Me.Controls.Add(Me.preciotxt)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.canttxt)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.codtxt)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.productotxt)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form1"
        Me.Text = "Formulario"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents productotxt As TextBox
    Friend WithEvents codtxt As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents canttxt As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents preciotxt As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents preciotxt2 As TextBox
    Friend WithEvents canttxt2 As TextBox
    Friend WithEvents codtxt2 As TextBox
    Friend WithEvents productotxt2 As TextBox
    Friend WithEvents preciotxt3 As TextBox
    Friend WithEvents canttxt3 As TextBox
    Friend WithEvents codtxt3 As TextBox
    Friend WithEvents productotxt3 As TextBox
    Friend WithEvents preciotxt4 As TextBox
    Friend WithEvents canttxt4 As TextBox
    Friend WithEvents codtxt4 As TextBox
    Friend WithEvents productotxt4 As TextBox
    Friend WithEvents semanatxt As ComboBox
    Friend WithEvents Button1 As Button
    Friend WithEvents totdesctxt As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents prectottxt As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents Label5 As Label
End Class
